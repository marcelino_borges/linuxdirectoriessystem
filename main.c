#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h> 
#include <errno.h> 
#include <locale.h>

int dirNameMaxSize = 20;
char mkDirNameLongErr[55] = "Nome do diretório muito longo, tente outro nome...";
char strExit[14] = "sair";			
char path[50], path2[50];
int len,lastIndex;

//ACENTOS: áÁéÉíÍóÓúÚ, âÂêÊîÎôÔûÛ, ãÃẽẼĩĨõÕũŨ, ÀàÈèÌìÒòÙù, çÇ, üÜ

int showMainMenu() {
	int option = -1;
	printf("\n------------------------------------------\n");
	printf("\nEscolha o que deseja fazer:");
	printf("\n0 - Sair");
	printf("\n1 - Criar Pasta");
	printf("\n2 - Deletar pasta");
	printf("\n3 - Deletar arquivo");
	printf("\n4 - Copiar arquivo");
	printf("\n5 - Mover arquivo");
	printf("\n6 - Exibir detalhes sobre um arquivo ou pasta");
	printf("\n8 - Listar conteúdo de uma pasta");
	printf("\n\n-----> SELECIONAR: ");

	scanf("%d", &option);

	return option; 
}

void createDir(char *path) {
	if (mkdir(path, S_IRUSR | S_IWUSR | S_IXUSR) != 0) {
     	printf("\n#### ERRO: %s\n", strerror(errno));      	
 	} else {
		printf("\n------------------------------------------\n");
		printf("\n@ Diretório criado com sucesso!");
		printf("\n------------------------------------------\n");
	}
}

void removeDir(char *path) {	
	/*
	int result = remove_all(path);
	qtyDeletedFiles = result;
	
	if(result != 0) {
		printf("#### ERRO: %s\n", strerror(errno)); 
	} else {
		printf("Diretorio deletado com sucesso!");
	}
	return result;
	*/
	char commands[57] = "rm -rf ";
	strcat(commands, path);
	
	int result = system(commands);
				
	if (result == -1) {
		printf("\nAlgo deu errado, tente novamente.\n");
	} else {
		printf("\nDiretório deletado com sucesso!\n");
	}
}

void removeUniqueFile(char *path) {	
	if(remove(path) != 0) {
		printf("\n------------------------------------------------\n");
		printf("\n@ Algo deu errado ao tentar deletar o arquivo...");
		printf("\n------------------------------------------------\n");
	} else {
		printf("\n------------------------------------------\n");
		printf("\n@ Arquivo deletado com sucesso!");
		printf("\n------------------------------------------\n");
	}
}

void removeAllFiles(char *path) {
	char commands[57] = "rm -rf ";
	strcat(commands, path);
	
	int result = system(commands);
	
	if (result == -1) {
		printf("\n---------------------------------------------------\n");
		printf("\n@ Algo deu errado ao tentar deletar os arquivos...");
		printf("\n---------------------------------------------------\n");
	} else {
		printf("\n------------------------------------------\n");
		printf("\n@ Arquivos deletados com sucesso!");
		printf("\n------------------------------------------\n");
	}
}

void copyFile(char *filePath, char *destPath) {
	char commands[57] = "cp -v ";
	strcat(commands, filePath);
	strcat(commands, " ");	
	strcat(commands, destPath);
	
	int result = system(commands);
	
	if (result == -1) {
		printf("\n---------------------------------------------------\n");
		printf("\n@ Algo deu errado ao tentar copiar o(s) arquivo(s)...");
		printf("\n---------------------------------------------------\n");
	} else {
		printf("\n------------------------------------------\n");
		printf("\n@ Arquivo(s) copiado(s) com sucesso!");
		printf("\n------------------------------------------\n");
	}	
}

void moveFile(char *filePath, char *destPath) {
	char commands[57] = "mv ";
	strcat(commands, filePath);
	strcat(commands, " ");	
	strcat(commands, destPath);
	
	int result = system(commands);
	
	if (result == -1) {
		printf("\n---------------------------------------------------\n");
		printf("\n@ Algo deu errado ao tentar mover o(s) arquivo(s)...");
		printf("\n---------------------------------------------------\n");
	} else {
		printf("\n------------------------------------------\n");
		printf("\n@ Arquivo(s) movido(s) com sucesso!");
		printf("\n------------------------------------------\n");
	}
}

void showDetails(char *filePath) {
	char commands[57] = "stat ";
	strcat(commands, filePath);
	
	printf("\n---------------------------------------------------\n");	
	int result = system(commands);
	
	if (result == -1) {
		printf("\n---------------------------------------------------\n");
		printf("\n@ Algo deu errado ao tentar exibir os detalhes do arquivo ou da pasta...");
		printf("\n---------------------------------------------------\n");
	} else {
		printf("\n------------------------------------------\n");
		printf("\n@ Detalhes do arquivo ou da pasta exibidos com sucesso!");
		printf("\n------------------------------------------\n");
	}
}

void listDirContent(char *path) {
	char commands[57] = "ls ";
	strcat(commands, path);
	
	printf("\n---------------------------------------------------\n");	
	int result = system(commands);
	
	if (result == -1) {
		printf("\n---------------------------------------------------\n");
		printf("\n@ Algo deu errado ao tentar listar o conteúdo do diretório...");
		printf("\n---------------------------------------------------\n");
	} else {
		printf("\n------------------------------------------\n");
		printf("\n@ Conteúdo do diretório listado com sucesso!");
	}
}

void main() {

	//Habilitando a acentuacao 
	setlocale(LC_ALL, "Portuguese"); 
	
	//Variável para decidir se permanece no menu principal 
	int quit = 0;
	
	//Menu principal
	while (!quit) {
		int option = showMainMenu();

		switch(option) {
			case 0:
				quit = 1;
				break;
			case 1:
				//1 - Criar Pasta
				printf("\nInforme o caminho/nome do diretório a ser criado (ex.: '/subpasta/subpasta/sua_nova_pasta') ou 'sair' para voltar ao menu principal:\n");
								
				scanf("%s", path);
				
				//Se o usuário digitar "sair", retorna ao menu principal
				if (strcmp(path, strExit) == 0) {
					break;
				}
				
				if (strlen(path) > dirNameMaxSize) {
					printf("\n#### ERRO: %s\n", mkDirNameLongErr);					
				} else {
					createDir(path);
				}
				
				break;
			case 2:
				//2 - Deletar pasta
				printf("\nInforme o caminho/nome do diretório a ser removido (ex.: '/subpasta/subpasta/sua_nova_pasta') ou 'sair' para voltar ao menu principal:\n");
								
				scanf("%s", path);
				
				//Se o usuário digitar "sair", retorna ao menu principal
				if (strcmp(path, strExit) == 0) {
					break;
				}
				
				removeDir(path);
				
				break;
			case 3:
				//3 - Deletar arquivo
				printf("\nInforme o caminho/nome do arquivo a ser removido (ex.: '/subpasta/subpasta/seu_arquivo.txt'), asterísco (ex.: '/subpasta/subpasta/*') para apagar todos os arquivos do diretório ou 'sair' para voltar ao menu principal:\n");
								
				scanf("%s", path);
				
				//Se o usuário digitar "sair", retorna ao menu principal
				if (strcmp(path, strExit) == 0) {
					break;
				}
				
				//Se o ultimo caractere da resposta do usuario for um *, entao o programa tenta apagar 
				//todos os arquivos do diretorio
				lastIndex = strlen(path) - 1;
				
				if (path[lastIndex] == '*') {
					removeAllFiles(path);
					break;
				}
				
				//removeUniqueFile(path);
				
				break;
			case 4:
				//4 - Copiar arquivo
				printf("\nInforme o caminho/nome do arquivo a ser copiado (ex.: '/subpasta/subpasta/seu_arquivo.txt') ou asterísco (ex.: '/subpasta/subpasta/*') para copiar todos os arquivos do diretório ou 'sair' para voltar ao menu principal:\n");
								
				scanf("%s", path);
				
				//Se o usuário digitar "sair", retorna ao menu principal
				if (strcmp(path, strExit) == 0) {
					break;
				}
				
				printf("\nInforme o caminho do diretório para onde deseja copiar o arquivo (ex.: '/subpasta/subpasta') ou 'sair' para voltar ao menu principal:\n");
								
				scanf("%s", path2);
				
				//Se o usuário digitar "sair", retorna ao menu principal
				if (strcmp(path, strExit) == 0) {
					break;
				}
				
				copyFile(path, path2);
				
				break;
			case 5:
				//5 - Mover arquivo
				
				printf("\nInforme o caminho/nome do arquivo a ser movido (ex.: '/subpasta/subpasta/seu_arquivo.txt') ou asterísco (ex.: '/subpasta/subpasta/*') para mover todos os arquivos do diretório ou 'sair' para voltar ao menu principal:\n");
								
				scanf("%s", path);
				
				//Se o usuário digitar "sair", retorna ao menu principal
				if (strcmp(path, strExit) == 0) {
					break;
				}
				
				printf("\nInforme o caminho do diretório para onde deseja mover o arquivo (ex.: '/subpasta/subpasta') ou 'sair' para voltar ao menu principal:\n");
								
				scanf("%s", path2);
				
				//Se o usuário digitar "sair", retorna ao menu principal
				if (strcmp(path, strExit) == 0) {
					break;
				}
				
				moveFile(path, path2);
				
				break;
			case 6:
				//6 - Detalhes sobre um arquivo
				
				printf("\nInforme o caminho/nome do arquivo ou da pasta para exibir seus detalhes (ex.: '/subpasta/subpasta/seu_arquivo.txt') ou 'sair' para voltar ao menu principal:\n");
								
				scanf("%s", path);
				
				//Se o usuário digitar "sair", retorna ao menu principal
				if (strcmp(path, strExit) == 0) {
					break;
				}
				
				showDetails(path);
				
				break;
			case 8:
				//8 - Listar conteudo de uma pasta
				
				printf("\nInforme o caminho/nome do diretório para exibir seu conteúdo (ex.: '/subpasta/subpasta/seu_arquivo') ou 'sair' para voltar ao menu principal:\n");
								
				scanf("%s", path);
				
				//Se o usuário digitar "sair", retorna ao menu principal
				if (strcmp(path, strExit) == 0) {
					break;
				}
				
				listDirContent(path);
				
				break;
			default:
				//-1 - Opcao nao reconhecida
				break;
		}

	}

	printf("\n\n|-----------------------------------------------------|");
	printf("\n|-------------> Obrigado! Volte sempre! <-------------|");
	printf("\n|-----------------------------------------------------|\n\n");
	exit(0); //Encerra e informa ao SO que finalizou com sucesso
}
